<?php
namespace App\Model;
use PDO;
use PDOException;

class Database {
    public $DBH;
    public $Host = "localhost";
    public $dbname = "atomic_project_36";
    public $user = "root";
    public $password = "";

    public function __construct(){
        try {
            $this->DBH = new PDO("mysql:host=$this->Host;dbname=$this->dbname", $this->user, $this->password);
            echo "connection successful <br>";
        }
        catch(PDOException $error){
            echo $error->getMessage();
        }


    }
}